# **Relazione finale attività progettuale Sicurezza dell'Informazione**

---

## Introduzione

L'attività progettuale 'Analisi statico/dinamica del protocollo Modbus' è stata inserita nell’ambito della ricerca di vulnerabilità insite nell’utilizzo, a livello industriale, di protocolli di comunicazione tra dispositivi e macchine connesse.
Lo scopo principale di tale attività è stato quello di individuare un insieme di conclusioni relative alla presenza/assenza dei requisiti di sicurezza fondamentali (confidenzialità, autenticità e integrità) con la successiva verifica dinamica degli stessi.

---

## Background

### Protocolli industriali e Modbus
Negli ultimi anni, con l’introduzione di nuove tecniche di distribuzione e di nuovi paradigmi di programmazione, molte aziende hanno deciso di inserire i loro interi processi produttivi all’interno di reti appositamente costruite: dispositivi come PLC (Programmable Logic Controller), HMI (Human Machine Interface), pannelli di controllo, driver, controllori di movimento, dispositivi di I/O, etc. possono utilizzare protocolli progettati ad hoc, per avviare sessioni di comunicazione.

Uno di questi protocolli di comunicazione è Modbus. Posizionandosi al settimo livello del modello ISO/OSI, infatti, esso permette l’interazione tra dispositivi connessi sfruttando una formattazione dei messaggi che li rende comprensibili agli estremi del canale comunicativo. In base a quale versione viene utilizzata (RTU, ASCII o TCP) Modbus definisce un PDU e un ADU caratteristici con campi utili sia per un indirizzamento dei pacchetti consistente sia per una costruzione coerente dei messaggi di richiesta/risposta/errore.

Dato il contesto applicativo, il rispetto dei requisiti di sicurezza, risulta essere fondamentale al fine di evitare danni, economici e non, all’apparato industriale e aziendale. Dall’analisi statica fatta, Modbus si è dimostrato molto vulnerabile da questo punto di vista essendo totalmente sprovvisto di meccanismi di riservatezza, autenticazione e integrità.

### ARP poisoning
Al fine di dimostrare dinamicamente la presenza di vulnerabilità, ci si è basati su un classico attacco informatico, conosciuto in letteratura con il nome di man in the middle attack: un’entità intrusa, stabilendo due connessioni con client e server, riesce a spacciarci per server e client, rispettivamente. In questo modo è in grado, non solo di accedere alla comunicazione tra i due (intercettando e inoltrando i messaggi di richiesta e di risposta) ma anche di modificarla, modellandola a suo piacimento.

Per permettere la creazione di due connessioni differenti, è fondamentale fare in modo che, i pacchetti provenienti dal client e destinati al server (e viceversa) vengano ricevuti dal middleman. L’indirizzamento dei pacchetti sulla rete avviene attraverso l’utilizzo di indirizzi IP anche se, l’instradamento a livello fisico, sfrutta i cosiddetti indirizzi MAC. Per risalire all’indirizzo fisico di un host, viene utilizzato il protocollo ARP (Address Resolution Protocol) che viene chiamato in causa nel momento in cui, un accoppiamento IP/MAC, risulta sconosciuto localmente (non presente in ARP tables) all’host mittente.

Iniettando nelle ARP tables di client e server degli accoppiamenti opportunamente costruiti, è possibile dirottare i pacchetti verso l’uomo in mezzo, accedendo finalmente alla comunicazione.

---

## Componenti del progetto

Per lo sviluppo di tale progetto sono stati utilizzati:

* [Vagrant](https://www.vagrantup.com/intro/index.html): strumento per la creazione e la gestione di macchine virtuali, grazie al quale sono state costruite le tre macchine rappresentati client, server e middleman rispettivamente. 
* [Pymodbus](https://pymodbus.readthedocs.io/en/latest/readme.html#summary): implementazione completa del protocollo Modbus in linguaggio python.
* [Scapy](https://scapy.readthedocs.io/en/latest/introduction.html): programma Python che consente all'utente di inviare, sniffare e falsificare pacchetti di rete.

---

## Attacco

### Analisi teorica
L'attacco descritto, si basa sull'esecuzione di tre punti principali:

1. modifica delle regole di indirizzamento a livello IP del middleman. In questo modo è possibile:

    * accogliere tutti i pacchetti destinati al server.
    * spacciare i propri pacchetti come forgiati dal client.
    
2. esecuzione dell’ARP poisoning in modo da completare il dirottamento dei pacchetti. All’interno dell’ARP Table del client si aggiunge una coppia IP/MAC contenente <IP server reale>/<MAC middleman>; all’interno dell’ARP table del server, invece, una coppia contenente <IP client reale>/<MAC middleman>.
3. Intercettamento, ricostruzione ed eventuale modifica dei pacchetti ModbusTcp provenienti dai corrispondenti in comunicazione.

### Configurazione ambiente
Dopo avere costruito le tre macchine virtuali e averle connesse alla stessa rete, è necessario:

* installare su client e server la libreria [Pymodbus](https://pymodbus.readthedocs.io/en/latest/readme.html#installing).
* caricare sulle macchine client, server e middleman i file client/ClientModbus.py, server/ModbusServer.py e middleman/Middleman.py rispettivamente.
* completare la configurazione per l'ARP poisoning nel file middleman/config.yaml:

    > master_IP: <ip client>
    > master_MAC: <MAC client>
    > slave_IP: <ip server>
    > slave_MAC: <MAC server>
    > fake_MAC: <MAC middleman>
    > port: <porta di ascolta del server>
    > d_rule: <ip server>
    > to_prerouting: <ip middleman>
    > to_postrouting: <ip client>

### Esecuzione
Dopo aver correttamente configurato l'ambiente e avviato le macchine, non resta che eseguire l'attacco. Recandosi nelle directory di salvataggio dei file caricati:

1. Avvio del server

     ```
    $ vagrant@server:~$ sudo python ModbusServer.py
    ```
    
2.  Avvio Middleman:

    ```
    $ vagrant@middleman:~$ sudo python Middleman.py
    ```
    
3. Avvio Client ed esecuzione attacco

    ```
    $ vagrant@client:~$ sudo python ClientModbus.py
    ```

Considerando che tutte le operazioni di modifica regole e ARP poisoning sono state integrate all'interno di Middleman.py, nel caso in cui qualcosa dovesse andare storto, è possibile verificare la corretta esecuzione della fase preliminare dell'attacco, lanciando:

* sulle macchine client e server:

    ```
    sudo arp
    ```
    
    per verificare l'avvenuta iniezione della coppia IP/MAC fasulla
    
* sulla macchina middleman:

    ```
    sudo iptables -t nat -L
    ```
    
    per verificare l'avvenuta aggiunta delle due regole di impacchettamento.

---

## Risultati e conclusioni

La verifica dinamica ha dato conferma delle conclusioni ottenute dall’analisi statica: il protocollo Modbus non prevede alcun meccanismo di confidenzialità, integrità e autenticità.

Innanzitutto, come è possibile notare nell’immagine sottostante raffigurante il traffico dell’host intruso, il Middleman è riuscito a stabilire due diverse connessioni, risultando server per il client e client per il server.

![connections](./imgs/connections.JPG)

In figura, possiamo notare il classico scambio SYN/ACK, promosso dal Three Way Handshake, per l’attivazione di connessioni TCP.

NB: i campi Source e Destination, nonostante si tratti del traffico riferito al Middleman (indirizzo IP 10.0.0.102), non presentano alcun riferimento ad esso. La totale assenza dell’host è legata all’aggiunta delle regole di gestione dei pacchetti all’ interno dell’IP table (come già detto nel paragrafo “Esecuzione”). Attraverso questa semplice sostituzione, il middleman riesce a scavalcare il meccanismo di autenticazione del ModbusTCP, implementato tramite la componente Acces Control del TCP Management Layer (assenza di autenticità).

Se, a livello di rete, i dati forniti potrebbero risultare inappropriati per un’attenta analisi, non lo sono a livello fisico. Spacchettando i segmenti No. 11 e No. 16, infatti, noteremo come:

* nel primo caso -> Src: MAC Client, Dst: MAC Middleman
* nel secondo -> Src: MAC Middleman, Dst: MAC Server

![firstReque](./imgs/firstReque.JPG)

Per quanto riguarda la confidenzialità, nell’immagine sottostante, è possibile notare come l’intruso riesca a ricostruire tutte le richieste pervenutegli dal client e tutte le corrispondenti risposte.

Al frame 14, infatti, subito dopo aver stabilito la connessione con il client, il Middleman riceve una richiesta di Write Single Register (Function Code: 6) che indica la volontà di scrivere, nel primo registro dell’unità 1 (slave), il valore 1. La stessa identica richiesta, viene presa e spedita così com’è verso il server subito dopo l’attivazione della seconda connessione presentata in precedenza (vedi No. 19 nell’immagine in fondo).

![frame14](./imgs/frame14.JPG)


La corrispondente risposta non tarda ad arrivare: al frame No. 21, infatti, il Middleman riceve un pacchetto Modbus che ricostruisce facilmente e spedisce direttamente al client (No. 24), facendone ripartire l’esecuzione.

![frame24](./imgs/frame24.JPG)

NB: l’operazione è andata a buon fine in quanto, il campo Function Code è stato riempito con lo stesso codice della richiesta. Da notare, inoltre, il riferimento stretto al Request Frame corrispondente.

Come ultima operazione richiesta dal client, è stata prevista la lettura di quello stesso registro modificato in precedenza. Per dimostrare l’assenza di meccanismi di integrità, l’intruso è stato messo nelle condizioni di modificare il messaggio, sostituendo al valore reale (1) con un valore fasullo (15862). In figura, sono messi a confronto i due messaggi di richiesta e di risposta.

![lastReqResp](./imgs/lastReqResp.JPG)

Se da un lato, abbiamo due richieste perfettamente uguali (a meno di sorgente e destinazione), dall’altro, i valori letti in Register Value risultano diversi tra di loro. La prima risposta, sulla sinistra, è quella reale, proveniente direttamente dal server; la seconda, sulla destra, è quella forgiata appositamente dal Middleman (analizzando opportunamente i frame, lo scambio di messaggi risulta chiaro).

Di seguito viene riportato l’intero dialogo tra le entità (dal punto di vista del Middleman):

![wholeComm](./imgs/wholeComm.JPG)
