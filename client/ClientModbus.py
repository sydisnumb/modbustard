#!/usr/bin/env python

from pymodbus.client.sync import ModbusTcpClient as ModbusClient

import logging
FORMAT = ('%(asctime)-15s %(threadName)-15s '
          '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.DEBUG)

UNIT = 0x01

def run_sync_client():
    
    client = ModbusClient('10.0.0.101', port=502, timeout=2)
    client.connect()

    #First Request (function code: 0x06
    log.debug("Write to a holding register and read back")
    rq = client.write_register(1, 1, unit=UNIT)
   
    # Second Request (function code: 0x03)
    rr = client.read_holding_registers(1, 1, unit=UNIT)
    assert(not rq.isError())
    log.debug("Returned value: %s", rr.registers[0])

    if rr.registers[0] > 10:
        log.debug("Temperature is too high! Machines are going to be stopped!")

    client.close()


if __name__ == "__main__":
    run_sync_client()
